var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const isOnline = require('is-online');
let isOnlineBool = false;

var Gpio = require('onoff').Gpio;
var relay1 = new Gpio(17, 'low');
var relay2 = new Gpio(27, 'low');
var MCTRL = new Gpio(22, 'low');

function endSound(court) {
  if(court == 0) {
    relay1.writeSync(0);
  } else {
    relay2.writeSync(0);
  }
}

function startSound(court) {
  if(court == 0) {
    relay1.writeSync(1);
  } else {
    relay2.writeSync(1);
  }
}

const screens = [
  {
    id: 0,
    team_one: {
      score: 0,
      team_name: "",
      team_uid: ""
    },
    team_two: {
      score: 0,
      team_name: "",
      team_uid: ""
    },
    status: "idle"
  },
  {
    id: 1,
    team_one: {
      score: 0,
      team_name: "",
      team_uid: ""
    },
    team_two: {
      score: 0,
      team_name: "",
      team_uid: ""
    },
    status: "idle",
    game_uid: ""
  }
];

io.on('connection', (socket) => {
  (async () => {
      io.emit("online", await isOnline());
  })();
  // Gamestatus changes to showTeams
  socket.on("preGame", game => {
    let screen_id = game.screen_id;
    screens[screen_id] = {
        id: screen_id,
        game_uid: game.uid,
        team_one: {
          score: 0,
          team_name: game.team_one_name,
          team_uid: game.team_one_uid
        },
        team_two: {
          score: 0,
          team_name: game.team_two_name,
          team_uid: game.team_two_uid
        },
        status: "showTeams",
        game_uid: game.game_uid
    }
    io.emit("scoreboards", screens);
  });

  // Gamestatus changes to showTeams
  socket.on("selectField", game => {
    let screen_id = game.screen_id;
    screens[screen_id] = {
      id: screen_id,
      team_one: {
        score: 0,
        team_name: "",
        team_uid: ""
      },
      team_two: {
        score: 0,
        team_name: "",
        team_uid: ""
      },
      status: "selectedField",
      game_uid: ""
    }
    io.emit("scoreboards", screens);
  });

  socket.on("undoSelectField", game => {
    let screen_id = game.screen_id;
    screens[screen_id] = {
      id: screen_id,
      team_one: {
        score: 0,
        team_name: "",
        team_uid: ""
      },
      team_two: {
        score: 0,
        team_name: "",
        team_uid: ""
      },
      status: "idle",
      game_uid: ""
    }
    io.emit("scoreboards", screens);
  });

  // Gamestatus changes to inGame
  socket.on("startGame", game => {
    let screen_id = game.screen_id;
    screens[screen_id].status = "inGame";
    io.emit("scoreboards", screens);
  });

  socket.on("returnHome", game => {
    let screen_id = game.screen_id;
    screens[screen_id].status = "idle";
    io.emit("scoreboards", screens);
  });

  socket.on("updatePoints", points => {
    let screen_id = points.screen_id;
    screens[screen_id].team_one.score = points.team_one_points;
    screens[screen_id].team_two.score = points.team_two_points;
    io.emit("scoreboards", screens);
  });

  socket.on("buzzer", screen => {
    let screen_id = screen.screen_id;
    buzzer(screen_id);
  });

  socket.on("endGame", screen_final => {
    let screen_id = screen_final.screen_id;
    screens[screen_id].status = "gameOver";
    io.emit("scoreboards", screens);
  });

  socket.on("isOnline", data => {
    io.emit("online", isOnlineBool);
  });

  socket.emit("scoreboards", screens);
});

function buzzer(court) {
  startSound(court);
  setTimeout(function() {
    endSound(court);
  },2000);
}

http.listen(3000, () => {
  console.log('Socket Server Running on port 3000');
  MCTRL.writeSync(1);
});
